﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class CreateHouse : MonoBehaviour {
	
	public GameObject house;
	public bool triggerButtonDown = false;
	public bool touchPadButton = false;
	GameObject newObj = null;
	GameObject rotationHouse = null;
	bool rotationHouseSelected = false;
	public bool newHouse = false;
	Vector3 newHousePosition;

	public float angle = 0f;

	private Vector2 axis = Vector2.zero;
	float touch_x;
	float touch_y;

	private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
	private Valve.VR.EVRButtonId touchButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;

	private SteamVR_Controller.Device controller {
		get { return SteamVR_Controller.Input((int)trackedObj.index);}
	}

	private SteamVR_TrackedObject trackedObj;

	// Use this for initialization
	void Start () {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}


	// Update is called once per frame
	void Update () {
		if (controller == null) {
			Debug.Log("Controller not initialized");
			return;		
		}
		triggerButtonDown = controller.GetPressDown(triggerButton);

		// Si on a sélectionné la maison à faire tourner
		if (rotationHouseSelected) {
			touchPadButton = controller.GetPressDown(touchButton);
			// Si on appuie sur le touchpad
			if (touchPadButton) {

				axis = controller.GetAxis();
				if (axis != Vector2.zero) {
					touch_x = axis.x;
					touch_y = axis.y;

					// On fait tourner la maison de 90° si on appuie sur le bouton droit ou gauche du touchpad
					if (touch_x < -0.25 && touch_y < 0.75 && touch_y > -0.75) {
						angle += 90f;
						rotationHouse.transform.Rotate (0f, angle%360, 0f);
					} else if (touch_x > 0.25 && touch_y < 0.75 && touch_y > -0.75) {
						angle -= 90f;
						rotationHouse.transform.Rotate (0f, angle%360, 0f);
					} 
				}
			}
		}
		// Si on a la nouvelle maison attachée à la manette
		if (newHouse) {
			// Si on appuie sur le trigger
			if (triggerButtonDown) {
				// On dépose la maison à l'endroit voulu
				newHousePosition = newObj.transform.position;
				this.newObj.transform.rotation = Quaternion.identity;
				// On dépose la maison sur le sol
				this.newObj.transform.position = new Vector3(newHousePosition.x, 2.0f, newHousePosition.z);
				//newObj.transform.Translate (Vector3.down * 40.0f * Time.deltaTime, Space.World);
				this.newObj.transform.parent = null;
				newHouse = false;
			}
		}
	}

	void OnTriggerEnter(Collider col) {
		// Si on rentre en collision avec la maison modèle
		if (col.gameObject.name == "House") {

			rotationHouseSelected = false;
			// On clone la maison modèle pour avoir une nouvelle maison attachée à la manette
			newObj = Instantiate (col.gameObject, this.transform);
			newObj.transform.localPosition = new Vector3 (0.5f, -0.2f, 1.5f);
			newObj.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);

			newHouse = true;
		} else {			
			// On sélectionne une des nouvelles maisons pour la faire tourner
			col.gameObject.name = "HouseSelected";
			rotationHouse = col.gameObject;
			rotationHouseSelected = true;
		}
	}
}
