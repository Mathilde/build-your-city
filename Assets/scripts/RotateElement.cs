﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateElement : MonoBehaviour {

    public float speed = 5.5f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		// On fait tourner la maison uniquement si c'est la maison modèle
		if (this.name == "House") {
			transform.Rotate (0f, speed, 0f);
		}
    }
}