![Polytech Paris-sud](https://www.usinenouvelle.com/mediatheque/3/4/0/000268043_image_260x175.jpg)

# Build Your City
Projet r�alis� en r�alit� virtuelle, par Imen Ammar Mohamed Salah Ben Taarit

## Insctruction pour lancer le projet
* Ouvrir le projet avec Unity
* Brancher le casque HTC VIVE
* Configurer la pi�ce avec Steam VR
* Lancer le projet


## Documentation
* Pour cloner la maison r�f�rente : toucher la maison avec la manette droite
* Pour d�poser la maison clon�e : appuyer sur le trigger de la manette droite
* Pour s�lectionner une des maisons clon�e : toucher la maison avec la manette droite
* Pour faire pivoter la maison selectionn�e : appuyer sur le trackpad (droite ou gauche)